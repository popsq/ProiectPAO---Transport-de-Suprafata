<html>
<head>
    <title>Proiect PAO - Popescu Andrei Bogdan - 331 - Emitere Card</title>
</head>
<link rel="stylesheet" type="text/css" href="interfata.css">
<script src="imports/jquery.min.js"></script>
<script src="imports/jquery.form.js"></script>
<script src="imports/moment.min.js"></script>
<script src="imports/transition.js"></script>
<script src="imports/collapse.js"></script>
<script src="imports/locales.min.js"></script>
<script src="imports/bootstrap.min.js"></script>
<link rel="stylesheet" href="imports/bootstrap.min.css">
<link rel="stylesheet" href="imports/bootstrap-datetimepicker.min.css">
<script src="imports/bootstrap-datetimepicker.min.js"></script>
<script src="main.js"></script>
<body>
<div>
    <div class="container-fluid">
        <div class="row">
            <div class="wrapper">
                <form id="emitere_card" action="/api/emitereCard" method="post" name="emitere_card"
                      class="form-input col-xs-4  col-md-offset-1">
                    <div class="col-md-12 modul-center">
                        <h3 class="input-header">Emitere Card</h3>
                        <input type="text" class="input" name="nume" placeholder="Nume" required="" autofocus=""/>
                        <input type="text" class="input" name="prenume" placeholder="Prenume" required=""/>
                        <button class="btn btn-lg btn-primary" name="submit" value="Emite Card" type="submit">Emite
                            Card
                        </button>
                    </div>
                </form>
            </div>
            <div class="wrapper">
                <form id="admin_card" action="/api/adminCard" method="post" name="admin_card"
                      class="form-input col-xs-4 col-md-offset-1">
                    <div class="col-md-12 modul-center">
                        <h3 class="input-header">Administrare Card</h3>
                        <input type="text" class="input" name="codCard" placeholder="Cod Card" required=""/>
                        <select id="dropdown_admin_card" class="form-control" type="dropdown" name="tipCard"
                                required="">
                            <option value="PORTOFEL" selected="selected">Incarcare Portofel Electronic</option>
                            <option value="ABONAMENT">Incarcare Abonament</option>
                        </select>
                        <input id="admin_card_suma" class="form-control" name="suma"
                               placeholder="Adauga suma"
                               value="3.00" type="number" min="3.00" max="50.00" step="0.01" required=""/>
                        <select id="dropdown_admin_durata" class="form-control" type="dropdown" name="durata"
                                style="display: none" required="">
                            <option value="1" selected="selected">O Zi</option>
                            <option value="30">O Luna</option>
                        </select>
                        <select id="admin_card_linie_dropdown" class="form-control" type="dropdown"
                                name="linie"
                                required="" style="display: none">
                            <option value="ALL" selected="selected">Toate Liniile</option>
                            <option value="1">1</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="11">11</option>
                            <option value="14">14</option>
                            <option value="16">16</option>
                            <option value="21">21</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="27">27</option>
                            <option value="32">32</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="40">40</option>
                            <option value="41">41</option>
                            <option value="42">42</option>
                            <option value="44">44</option>
                            <option value="45">45</option>
                            <option value="46">46</option>
                            <option value="47">47</option>
                            <option value="56">56</option>
                            <option value="61">61</option>
                            <option value="62">62</option>
                            <option value="65">65</option>
                            <option value="66">66</option>
                            <option value="69">69</option>
                            <option value="70">70</option>
                            <option value="73">73</option>
                            <option value="77">77</option>
                            <option value="79">79</option>
                            <option value="85">85</option>
                            <option value="86">86</option>
                            <option value="90">90</option>
                            <option value="91">91</option>
                            <option value="93">93</option>
                            <option value="96">96</option>
                            <option value="101">101</option>
                            <option value="102">102</option>
                            <option value="103">103</option>
                            <option value="104">104</option>
                            <option value="105">105</option>
                            <option value="106">106</option>
                            <option value="112">112</option>
                            <option value="116">116</option>
                            <option value="117">117</option>
                            <option value="122">122</option>
                            <option value="123">123</option>
                            <option value="124">124</option>
                            <option value="125">125</option>
                            <option value="126">126</option>
                            <option value="131">131</option>
                            <option value="133">133</option>
                            <option value="135">135</option>
                            <option value="136">136</option>
                            <option value="137">137</option>
                            <option value="138">138</option>
                            <option value="139">139</option>
                            <option value="141">141</option>
                            <option value="143">143</option>
                            <option value="149">149</option>
                            <option value="162">162</option>
                            <option value="163">163</option>
                            <option value="168">168</option>
                            <option value="173">173</option>
                            <option value="178">178</option>
                            <option value="182">182</option>
                            <option value="185">185</option>
                            <option value="202">202</option>
                            <option value="205">205</option>
                            <option value="216">216</option>
                            <option value="220">220</option>
                            <option value="221">221</option>
                            <option value="222">222</option>
                            <option value="223">223</option>
                            <option value="226">226</option>
                            <option value="227">227</option>
                            <option value="232">232</option>
                            <option value="236">236</option>
                            <option value="243">243</option>
                            <option value="246">246</option>
                            <option value="253">253</option>
                            <option value="261">261</option>
                            <option value="268">268</option>
                            <option value="282">282</option>
                            <option value="300">300</option>
                            <option value="301">301</option>
                            <option value="302">302</option>
                            <option value="303">303</option>
                            <option value="304">304</option>
                            <option value="311">311</option>
                            <option value="312">312</option>
                            <option value="313">313</option>
                            <option value="323">323</option>
                            <option value="330">330</option>
                            <option value="331">331</option>
                            <option value="335">335</option>
                            <option value="336">336</option>
                            <option value="368">368</option>
                            <option value="381">381</option>
                            <option value="385">385</option>
                            <option value="601">601</option>
                            <option value="611">611</option>
                            <option value="634">634</option>
                            <option value="655">655</option>
                            <option value="668">668</option>
                            <option value="682">682</option>
                            <option value="696">696</option>
                            <option value="697">697</option>
                        </select>
                        <button class="btn btn-lg btn-primary center" name="submit" value="Admin Card" type="submit">
                            Administreaza Card
                        </button>
                    </div>
                </form>
            </div>
            <div class="wrapper">
                <form id="validare_card" action="/api/validareCard" method="post" name="validare_card"
                      class="form-input col-xs-4 col-md-offset-1">
                    <div class="col-md-12 modul-center">
                        <h3 class="input-header">Validare Card</h3>
                        <input type="text" class="input" name="codCard" placeholder="Cod Card" required=""/>
                        <div class='col-sm-6'>
                            <div class="form-group">
                                <div class='input-group date' id="validare_data">
                                    <input type='text' name="dataValidare" class="form-control" required=""/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#validare_data').datetimepicker({
                                        defaultDate: 'now',
                                        format: 'YYYY-DD-MM HH:mm'
                                    });
                                });
                            </script>
                        </div>
                        <input id="validare_cod_masina" class="form-control" name="codMasina" placeholder="Cod Masina"
                               type="number" maxlength="10" step="any" min="0" required=""/>
                        <select id="validare_card_linie_dropdown" class="form-control" type="dropdown"
                                name="linie"
                                required="">
                            <option value="1">1</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="11">11</option>
                            <option value="14">14</option>
                            <option value="16">16</option>
                            <option value="21">21</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="27">27</option>
                            <option value="32">32</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="40">40</option>
                            <option value="41">41</option>
                            <option value="42">42</option>
                            <option value="44">44</option>
                            <option value="45">45</option>
                            <option value="46">46</option>
                            <option value="47">47</option>
                            <option value="56">56</option>
                            <option value="61">61</option>
                            <option value="62">62</option>
                            <option value="65">65</option>
                            <option value="66">66</option>
                            <option value="69">69</option>
                            <option value="70">70</option>
                            <option value="73">73</option>
                            <option value="77">77</option>
                            <option value="79">79</option>
                            <option value="85">85</option>
                            <option value="86">86</option>
                            <option value="90">90</option>
                            <option value="91">91</option>
                            <option value="93">93</option>
                            <option value="96">96</option>
                            <option value="101">101</option>
                            <option value="102">102</option>
                            <option value="103">103</option>
                            <option value="104">104</option>
                            <option value="105">105</option>
                            <option value="106">106</option>
                            <option value="112">112</option>
                            <option value="116">116</option>
                            <option value="117">117</option>
                            <option value="122">122</option>
                            <option value="123">123</option>
                            <option value="124">124</option>
                            <option value="125">125</option>
                            <option value="126">126</option>
                            <option value="131">131</option>
                            <option value="133">133</option>
                            <option value="135">135</option>
                            <option value="136">136</option>
                            <option value="137">137</option>
                            <option value="138">138</option>
                            <option value="139">139</option>
                            <option value="141">141</option>
                            <option value="143">143</option>
                            <option value="149">149</option>
                            <option value="162">162</option>
                            <option value="163">163</option>
                            <option value="168">168</option>
                            <option value="173">173</option>
                            <option value="178">178</option>
                            <option value="182">182</option>
                            <option value="185">185</option>
                            <option value="202">202</option>
                            <option value="205">205</option>
                            <option value="216">216</option>
                            <option value="220">220</option>
                            <option value="221">221</option>
                            <option value="222">222</option>
                            <option value="223">223</option>
                            <option value="226">226</option>
                            <option value="227">227</option>
                            <option value="232">232</option>
                            <option value="236">236</option>
                            <option value="243">243</option>
                            <option value="246">246</option>
                            <option value="253">253</option>
                            <option value="261">261</option>
                            <option value="268">268</option>
                            <option value="282">282</option>
                            <option value="300">300</option>
                            <option value="301">301</option>
                            <option value="302">302</option>
                            <option value="303">303</option>
                            <option value="304">304</option>
                            <option value="311">311</option>
                            <option value="312">312</option>
                            <option value="313">313</option>
                            <option value="323">323</option>
                            <option value="330">330</option>
                            <option value="331">331</option>
                            <option value="335">335</option>
                            <option value="336">336</option>
                            <option value="368">368</option>
                            <option value="381">381</option>
                            <option value="385">385</option>
                            <option value="601">601</option>
                            <option value="611">611</option>
                            <option value="634">634</option>
                            <option value="655">655</option>
                            <option value="668">668</option>
                            <option value="682">682</option>
                            <option value="696">696</option>
                            <option value="697">697</option>
                        </select>
                        <button id="validare" class="btn btn-lg btn-primary" name="submit" value="Validare" type="submit">
                            Validare
                        </button>
                    </div>
                </form>
            </div>
            <div class="wrapper">
                <form id="verificare_card" action="/api/verificareCard" method="post" name="verificare_card"
                      class="form-input col-xs-4 col-md-offset-1">
                    <div class="col-md-12 modul-center">
                        <h3 class="input-header">Verificare Card</h3>
                        <input type="text" class="input" name="codCard" placeholder="Cod Card" required=""/>
                        <div class='col-sm-6'>
                            <div class="form-group">
                                <div class='input-group date' id="verificare_data">
                                    <input type='text' name="dataValidare" class="form-control" required=""/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#verificare_data').datetimepicker({
                                        defaultDate: 'now',
                                        format: 'YYYY-DD-MM HH:mm'
                                    });
                                });
                            </script>
                        </div>
                        <input id="verificare_cod_masina" class="form-control" name="codMasina" placeholder="Cod Masina"
                               type="number" maxlength="10" step="any" min="0" required=""/>
                        <select id="verificare_card_linie_dropdown" class="form-control" type="dropdown"
                                name="linie"
                                required="">
                            <option value="1">1</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="11">11</option>
                            <option value="14">14</option>
                            <option value="16">16</option>
                            <option value="21">21</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="27">27</option>
                            <option value="32">32</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="40">40</option>
                            <option value="41">41</option>
                            <option value="42">42</option>
                            <option value="44">44</option>
                            <option value="45">45</option>
                            <option value="46">46</option>
                            <option value="47">47</option>
                            <option value="56">56</option>
                            <option value="61">61</option>
                            <option value="62">62</option>
                            <option value="65">65</option>
                            <option value="66">66</option>
                            <option value="69">69</option>
                            <option value="70">70</option>
                            <option value="73">73</option>
                            <option value="77">77</option>
                            <option value="79">79</option>
                            <option value="85">85</option>
                            <option value="86">86</option>
                            <option value="90">90</option>
                            <option value="91">91</option>
                            <option value="93">93</option>
                            <option value="96">96</option>
                            <option value="101">101</option>
                            <option value="102">102</option>
                            <option value="103">103</option>
                            <option value="104">104</option>
                            <option value="105">105</option>
                            <option value="106">106</option>
                            <option value="112">112</option>
                            <option value="116">116</option>
                            <option value="117">117</option>
                            <option value="122">122</option>
                            <option value="123">123</option>
                            <option value="124">124</option>
                            <option value="125">125</option>
                            <option value="126">126</option>
                            <option value="131">131</option>
                            <option value="133">133</option>
                            <option value="135">135</option>
                            <option value="136">136</option>
                            <option value="137">137</option>
                            <option value="138">138</option>
                            <option value="139">139</option>
                            <option value="141">141</option>
                            <option value="143">143</option>
                            <option value="149">149</option>
                            <option value="162">162</option>
                            <option value="163">163</option>
                            <option value="168">168</option>
                            <option value="173">173</option>
                            <option value="178">178</option>
                            <option value="182">182</option>
                            <option value="185">185</option>
                            <option value="202">202</option>
                            <option value="205">205</option>
                            <option value="216">216</option>
                            <option value="220">220</option>
                            <option value="221">221</option>
                            <option value="222">222</option>
                            <option value="223">223</option>
                            <option value="226">226</option>
                            <option value="227">227</option>
                            <option value="232">232</option>
                            <option value="236">236</option>
                            <option value="243">243</option>
                            <option value="246">246</option>
                            <option value="253">253</option>
                            <option value="261">261</option>
                            <option value="268">268</option>
                            <option value="282">282</option>
                            <option value="300">300</option>
                            <option value="301">301</option>
                            <option value="302">302</option>
                            <option value="303">303</option>
                            <option value="304">304</option>
                            <option value="311">311</option>
                            <option value="312">312</option>
                            <option value="313">313</option>
                            <option value="323">323</option>
                            <option value="330">330</option>
                            <option value="331">331</option>
                            <option value="335">335</option>
                            <option value="336">336</option>
                            <option value="368">368</option>
                            <option value="381">381</option>
                            <option value="385">385</option>
                            <option value="601">601</option>
                            <option value="611">611</option>
                            <option value="634">634</option>
                            <option value="655">655</option>
                            <option value="668">668</option>
                            <option value="682">682</option>
                            <option value="696">696</option>
                            <option value="697">697</option>
                        </select>
                        <button id="verificare" class="btn btn-lg btn-primary" name="submit" value="Verificare" type="submit">
                            Verificare
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="wrapper">
            <div id="form_response" class="response-output" style="display: none"></div>
        </div>
    </div>
</div>
</body>
</html>