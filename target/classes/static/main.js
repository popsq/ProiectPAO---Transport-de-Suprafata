$(document).ready(function () {
    $('#dropdown_admin_card').on('change', function () {
        if (this.value == 'PORTOFEL') {
            $("#admin_card_linie_dropdown").hide();
            $("#dropdown_admin_durata").hide();
            $("#admin_card_suma").show();
        } else {
            $("#admin_card_linie_dropdown").show();
            $("#dropdown_admin_durata").show();
            $("#admin_card_suma").hide();
        }
    });
});

$(document).on("submit", "#validare_card", function (event) {
    var $form = $(this);

    $.post($form.attr("action"), $form.serialize(), function (response) {
        $("#form_response")
            .show()
            .html("<div id='validare_card' class=''>" + response + "<div>");
    });

    event.preventDefault();
});

$(document).on("submit", "#verificare_card", function (event) {
    var $form = $(this);

    $.post($form.attr("action"), $form.serialize(), function (response) {
        $("#form_response")
            .show()
            .html("<div id='verificare_card' class=''>" + response + "<div>");
    });

    event.preventDefault();
});

$(document).on("submit", "#emitere_card", function (event) {
    var $form = $(this);

    $.post($form.attr("action"), $form.serialize(), function (response) {
        $("#form_response")
            .show()
            .html("<div id='cod_card' class=''>" + response + "<div>");
    });

    event.preventDefault();
});

$(document).on("submit", "#admin_card", function (event) {
    var $form = $(this);

    $.post($form.attr("action"), $form.serialize(), function (response) {
        $("#form_response")
            .show()
            .html("<div id='admin_card' class=''>" + response + "<div>");
    });

    event.preventDefault();
});