package com.andrei.pao.controller.servlet;

import com.andrei.pao.exceptions.InvalidInputException;
import com.andrei.pao.db.dao.CardCRUD;
import com.andrei.pao.db.dbo.Card;
import com.andrei.pao.db.dbo.EntityBuilder;
import com.andrei.pao.model.CardOpType;
import com.andrei.pao.utils.LogProvider;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@WebServlet(urlPatterns = "/emitereCard")
public class EmitereCardServlet extends HttpServlet implements LogProvider {

    private final CardCRUD cardCRUD = new CardCRUD();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletOutputStream os = resp.getOutputStream();

        Card dateInitiale;

        try {
            try {
                dateInitiale = EntityBuilder.buildCard(req.getParameter("nume").trim(), req.getParameter("prenume").trim());
            }catch (Exception e){
                throw new InvalidInputException("Date incomplete. Reintroduceti.");
            }

            Optional<Card> cardOptional = Optional.of(
                    cardCRUD.operateCard(
                            dateInitiale,
                            CardOpType.EMITERE
                    )
            );

            if (cardOptional.isPresent()) {
                Card cardData = cardOptional.get();
                os.print(new StringBuilder("Card Emis! Client: ")
                        .append(cardData.getNume())
                        .append(" ")
                        .append(cardData.getPrenume())
                        .append(" Cod Card: ")
                        .append(cardData.getCodCard())
                        .toString());
            }

        } catch (SQLException sqle) {
            logger().error("Nu s-a putut stabili conexiunea la baza de date. Eroare: {}", sqle.getMessage());
            os.print("Nu s-a putut stabili conexiunea la baza de date. Eroare: " + sqle.getMessage());
            sqle.printStackTrace();
        }catch (InvalidInputException iie){
            os.print("Emitere Card! Eroare: " + iie.getMessage());
        } finally {
            this.getServletContext().getContext("/").getRequestDispatcher("/index.jsp");
            os.close();
        }
    }
}
