package com.andrei.pao.controller.servlet;

import com.andrei.pao.exceptions.InvalidInputException;
import com.andrei.pao.db.dao.CardCRUD;
import com.andrei.pao.db.dao.ValidatCRUD;
import com.andrei.pao.db.dbo.Card;
import com.andrei.pao.db.dbo.EntityBuilder;
import com.andrei.pao.db.dbo.Validare;
import com.andrei.pao.model.CardOpType;
import com.andrei.pao.model.CardType;
import com.andrei.pao.model.ValidareOpType;
import com.andrei.pao.utils.LogProvider;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@WebServlet("/validareCard")
public class ValidareCardServlet extends HttpServlet implements LogProvider {

    private final CardCRUD cardCRUD = new CardCRUD();
    private final ValidatCRUD validareCRUD = new ValidatCRUD();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletOutputStream os = resp.getOutputStream();

        LocalDateTime dataValidare;
        String codMasina;
        String linie;
        Card cardData;

        boolean validareCuPortofel = false;
        StringBuilder responseBuilder = new StringBuilder();

        try {
            try {
                cardData = EntityBuilder.buildCard(req.getParameter("codCard").trim());
                codMasina = req.getParameter("codMasina").trim();
                linie = req.getParameter("linie");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-dd-MM HH:mm");
                dataValidare = LocalDateTime.parse(req.getParameter("dataValidare").trim(), formatter);
            } catch (Exception e) {
                throw new InvalidInputException("Date incorecte. Reintroduceti");
            }


            Optional<Card> cardOptional = Optional.of(
                    cardCRUD.operateCard(
                            cardData,
                            CardOpType.GET_CARD
                    )
            );

            if (cardOptional.isPresent()) {
                cardData = cardOptional.get();
                if (cardData.getTipCard().equals(CardType.ABONAMENT.name())) {
                    if (dataValidare.isAfter(cardData.getDataExpirareAbonament())) {
                        if (cardData.getSumaDisponibila() >= 1.3) {
                            validareCuPortofel = true;
                            cardData.setSumaDisponibila(cardData.getSumaDisponibila() - 1.3);
                            responseBuilder.append("Validare Card! Abonament expirat, dar portofel disponibil. Retragere 1.3 lei din fondurile disponibile.");
                            responseBuilder.append(" Sold disponibil: ").append(new DecimalFormat(".##").format(cardData.getSumaDisponibila()));
                        } else {
                            throw new InvalidInputException("Abonament expirat si suma insuficienta in portofel.");
                        }
                    } else {
                        if (!cardData.getTipLinie().equals("ALL")) {
                            if (cardData.getTipLinie().equals(linie)) {
                                responseBuilder.append("Validare Card! Abonament Gasit. Card validat.");
                            } else {
                                throw new InvalidInputException("Abonament gasit, dar validat pe o linie incorecta. Linia pe care este facut abonamentul este " + cardData.getTipLinie());
                            }
                        } else {
                           responseBuilder.append("Validare Card! Abonament Gasit. Card validat.");
                        }
                    }
                } else {
                    validareCuPortofel = true;
                    if (cardData.getSumaDisponibila() >= 1.3) {
                        cardData.setSumaDisponibila(cardData.getSumaDisponibila() - 1.3);
                        responseBuilder.append("Validare Card! Portofel disponibil. Retragere 1.3 lei din fondurile disponibile.");
                        responseBuilder.append(" Sold disponibil: ").append(new DecimalFormat(".##").format(cardData.getSumaDisponibila()));
                    } else {
                        throw new InvalidInputException("Suma insuficienta in portofel.");
                    }
                }

                Validare validare = EntityBuilder.buildValidare(cardData.getCodCard(), dataValidare, codMasina, linie);

                validareCRUD.validateIfDataIsCorrect(validare, validareCuPortofel);
                validareCRUD.operateValidare(
                        EntityBuilder.buildValidare(cardData.getCodCard(), dataValidare, codMasina, linie),
                        ValidareOpType.ADD_VALIDARE
                );

                if (validareCuPortofel) {
                    cardCRUD.operateCard(
                            cardData,
                            CardOpType.PORTOFEL
                    );
                }

                os.print(responseBuilder.toString());
            }
        } catch (SQLException sqle) {
            logger().error("Nu s-a putut stabili conexiunea la baza de date. Eroare: {}", sqle.getMessage());
            os.print("Nu s-a putut stabili conexiunea la baza de date. Eroare: " + sqle.getMessage());
            sqle.printStackTrace();
        } catch (InvalidInputException iie) {
            os.print("Validare Card! Eroare: " + iie.getMessage());
        } finally {
            this.getServletContext().getContext("/").getRequestDispatcher("/index.jsp");
            os.close();
        }
    }
}
