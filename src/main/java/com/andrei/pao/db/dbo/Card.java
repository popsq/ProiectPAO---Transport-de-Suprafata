package com.andrei.pao.db.dbo;

import java.time.LocalDateTime;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class Card {

    private String codCard;
    private String nume;
    private String prenume;
    private LocalDateTime dataEmitere;
    private LocalDateTime dataExpirareAbonament;
    private String tipCard;
    private String tipLinie;
    private double sumaDisponibila;

    public Card(String nume, String prenume) {
        this.nume = nume;
        this.prenume = prenume;
    }

    public Card(String codCard) {
        this.codCard = codCard;
    }

    public Card() {

    }

    public String getCodCard() {
        return codCard;
    }

    public void setCodCard(String codCard) {
        this.codCard = codCard;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public LocalDateTime getDataEmitere() {
        return dataEmitere;
    }

    public void setDataEmitere(LocalDateTime dataEmitere) {
        this.dataEmitere = dataEmitere;
    }

    public LocalDateTime getDataExpirareAbonament() {
        return dataExpirareAbonament;
    }

    public void setDataExpirareAbonament(LocalDateTime dataExpirareAbonament) {
        this.dataExpirareAbonament = dataExpirareAbonament;
    }

    public String getTipCard() {
        return tipCard;
    }

    public void setTipCard(String tipCard) {
        this.tipCard = tipCard;
    }

    public double getSumaDisponibila() {
        return sumaDisponibila;
    }

    public void setSumaDisponibila(double sumaDisponibila) {
        this.sumaDisponibila = sumaDisponibila;
    }

    public String getTipLinie() {
        return tipLinie;
    }

    public void setTipLinie(String tipLinie) {
        this.tipLinie = tipLinie;
    }
}
