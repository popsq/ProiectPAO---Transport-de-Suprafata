package com.andrei.pao.db.dao;

import com.andrei.pao.db.DatabaseConnectionFactory;
import com.andrei.pao.db.dbo.Card;
import com.andrei.pao.model.CardOpType;
import com.andrei.pao.utils.LogProvider;

import java.io.Serializable;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class CardCRUD implements LogProvider, Serializable {

    private final String INSERT_CARD_QUERY = "INSERT INTO tcard" +
            "(nume, prenume)" +
            " VALUES " +
            "(?,?)" +
            " RETURNING *;";

    private final String MODIFY_ABONAMENT_CARD_QUERY = "UPDATE tcard SET " +
            "data_expirare_abonament = ?," +
            "tip_card = ?," +
            "tip_linie = ? " +
            "WHERE cod_card = ? " +
            "RETURNING *;";

    private final String MODIFY_PORTOFEL_CARD_QUERY = "UPDATE tcard SET " +
            "suma_disponibila = ? " +
            "WHERE cod_card = ? " +
            "RETURNING *;";

    private final String GET_CARD_QUERY = "SELECT * " +
            "FROM tcard " +
            "WHERE cod_card = ?;";

    public Card operateCard(Card card, CardOpType operationType) throws SQLException {
        try (
                Connection connection = DatabaseConnectionFactory.getNewConnection();
                PreparedStatement preparedStatement = defineStatement(connection, operationType, card)
        ) {
            Optional<PreparedStatement> optionalPreparedStatement = Optional.ofNullable(preparedStatement);
            if (optionalPreparedStatement.isPresent()) {
                optionalPreparedStatement.get().execute();

                card = updateCardFromResultSet(card, optionalPreparedStatement.get().getResultSet());

                if (operationType == CardOpType.GET_CARD && (card.getNume() == null || card.getPrenume() == null)){
                    throw new SQLException("Cod card invalid. Reintroduceti.");
                }
            }

            return card;
        }
    }

    private Card updateCardFromResultSet(Card card, ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            Timestamp dataEmitere = resultSet.getTimestamp("data_emitere");
            Timestamp dataExpirareAbonament = resultSet.getTimestamp("data_expirare_abonament");

            card.setCodCard(String.valueOf(resultSet.getLong("cod_card")));
            card.setNume(resultSet.getString("nume"));
            card.setPrenume(resultSet.getString("prenume"));
            card.setTipCard(resultSet.getString("tip_card"));
            card.setTipLinie(resultSet.getString("tip_linie"));
            card.setSumaDisponibila(resultSet.getDouble("suma_disponibila"));

            if (dataEmitere != null) {
                card.setDataEmitere(LocalDateTime.ofInstant(Instant.ofEpochMilli(dataEmitere.getTime()), ZoneId.systemDefault()));
            }
            if (dataExpirareAbonament != null) {
                card.setDataExpirareAbonament(LocalDateTime.ofInstant(Instant.ofEpochMilli(dataExpirareAbonament.getTime()), ZoneId.systemDefault()));
            }

            resultSet.close();
        }
        return card;
    }

    private PreparedStatement defineStatement(Connection connection, CardOpType operationType, Card card) throws SQLException {
        PreparedStatement preparedStatement;
        switch (operationType) {
            case EMITERE:
                preparedStatement = connection.prepareStatement(INSERT_CARD_QUERY);
                preparedStatement.setString(1, card.getNume());
                preparedStatement.setString(2, card.getPrenume());

                return preparedStatement;
            case ABONAMENT:
                preparedStatement = connection.prepareStatement(MODIFY_ABONAMENT_CARD_QUERY);
                preparedStatement.setTimestamp(1, new Timestamp(card.getDataExpirareAbonament().toInstant(ZoneId.systemDefault().getRules().getOffset(Instant.now())).toEpochMilli()));
                preparedStatement.setString(2, card.getTipCard());
                preparedStatement.setString(3, card.getTipLinie());
                preparedStatement.setLong(4, Long.valueOf(card.getCodCard()));

                return preparedStatement;
            case PORTOFEL:
                preparedStatement = connection.prepareStatement(MODIFY_PORTOFEL_CARD_QUERY);
                preparedStatement.setDouble(1, card.getSumaDisponibila());
                preparedStatement.setLong(2, Long.valueOf(card.getCodCard()));

                return preparedStatement;
            case GET_CARD:
                preparedStatement = connection.prepareStatement(GET_CARD_QUERY);
                preparedStatement.setLong(1, Long.valueOf(card.getCodCard()));

                return preparedStatement;
            default:
                return null;
        }
    }
}
