package com.andrei.pao;

import com.andrei.pao.controller.servlet.*;
import com.andrei.pao.utils.ServerProperties;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class Main {
    private static Server server = new Server();

    public static void main(String[] args) throws Exception {

        ServerProperties.getInstance().loadProperties();

        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8080);
        server.addConnector(connector);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        context.setResourceBase("target/classes/static");
        context.setWelcomeFiles(new String[]{"index.jsp"});

        ServletContextHandler wsContext = new ServletContextHandler();
        wsContext.setContextPath("/api");
        wsContext.addServlet(EmitereCardServlet.class,"/emitereCard");
        wsContext.addServlet(AdministrareCardServlet.class,"/adminCard");
        wsContext.addServlet(ValidareCardServlet.class,"/validareCard");
        wsContext.addServlet(VerificareCardServlet.class,"/verificareCard");
        wsContext.addServlet(StopJettyServlet.class,"/stopServer");


        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[]{context, wsContext});

        server.setHandler(contexts);

        context.addServlet(DefaultServlet.class, "/");

        Thread stopJettyThread = new StopJetty();
        stopJettyThread.start();
        server.start();
        server.join();

    }

    private static class StopJetty extends Thread {

        private ServerSocket socket;

        public StopJetty() {
            setDaemon(true);
            try {
                socket = new ServerSocket(8081, 1, InetAddress.getByName("127.0.0.1"));
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void run() {
            Socket accept;
            try {
                accept = socket.accept();
                BufferedReader reader = new BufferedReader(new InputStreamReader(accept.getInputStream()));
                reader.readLine();
                server.stop();
                accept.close();
                socket.close();
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
