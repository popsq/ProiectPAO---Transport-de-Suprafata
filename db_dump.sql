--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

-- Started on 2017-05-26 18:39:07

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 16409)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2148 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 16414)
-- Name: tcard; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tcard (
    nume character varying,
    prenume character varying,
    cod_card bigint NOT NULL,
    data_emitere timestamp with time zone DEFAULT timezone('utc'::text, now()),
    data_expirare_abonament timestamp with time zone,
    tip_card character varying DEFAULT 'PORTOFEL'::character varying,
    tip_linie character varying,
    suma_disponibila numeric(10,2) DEFAULT 0.00 NOT NULL
);


ALTER TABLE tcard OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16423)
-- Name: tcard_cod_card_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcard_cod_card_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tcard_cod_card_seq OWNER TO postgres;

--
-- TOC entry 2149 (class 0 OID 0)
-- Dependencies: 186
-- Name: tcard_cod_card_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcard_cod_card_seq OWNED BY tcard.cod_card;


--
-- TOC entry 187 (class 1259 OID 16425)
-- Name: tvalidare; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tvalidare (
    data_validare timestamp with time zone NOT NULL,
    id_validare bigint NOT NULL,
    cod_card bigint NOT NULL,
    cod_masina bigint NOT NULL,
    linie character varying NOT NULL
);


ALTER TABLE tvalidare OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 16431)
-- Name: tvalidat_id_validare_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tvalidat_id_validare_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tvalidat_id_validare_seq OWNER TO postgres;

--
-- TOC entry 2150 (class 0 OID 0)
-- Dependencies: 188
-- Name: tvalidat_id_validare_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tvalidat_id_validare_seq OWNED BY tvalidare.id_validare;


--
-- TOC entry 2012 (class 2604 OID 16445)
-- Name: tcard cod_card; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcard ALTER COLUMN cod_card SET DEFAULT nextval('tcard_cod_card_seq'::regclass);


--
-- TOC entry 2013 (class 2604 OID 16446)
-- Name: tvalidare id_validare; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tvalidare ALTER COLUMN id_validare SET DEFAULT nextval('tvalidat_id_validare_seq'::regclass);


--
-- TOC entry 2137 (class 0 OID 16414)
-- Dependencies: 185
-- Data for Name: tcard; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcard (nume, prenume, cod_card, data_emitere, data_expirare_abonament, tip_card, tip_linie, suma_disponibila) FROM stdin;
Andrei	Popescu	10000000002	2017-05-13 17:40:30.425569+03	\N	PORTOFEL	\N	0.00
\N	\N	10000000004	2017-05-14 00:09:30.351381+03	\N	PORTOFEL	\N	4.00
Test	Test	10000000005	2017-05-15 16:32:50.164772+03	\N	PORTOFEL	\N	0.00
Andrei	Popescu	10000000003	2017-05-13 23:54:22.053864+03	\N	PORTOFEL	\N	-2.00
Andrei	Popescu	10000000006	2017-05-15 17:59:04.888367+03	2017-05-16 21:23:02.119+03	ABONAMENT	25	50.00
test	Test	10000000007	2017-05-15 19:31:18.764789+03	\N	PORTOFEL	\N	0.00
dsa	dsa	10000000009	2017-05-16 20:14:18.219392+03	\N	PORTOFEL	\N	0.00
w	s	10000000010	2017-05-16 20:40:10.813698+03	\N	PORTOFEL	\N	0.00
Test	Test	10000000008	2017-05-16 19:58:39.687958+03	2017-05-17 22:58:47.38+03	ABONAMENT	ALL	38.00
test	test	10000000011	2017-05-16 21:33:54.510036+03	\N	PORTOFEL	\N	1.00
Popescu	Andrei	10000000012	2017-05-24 19:37:10.785147+03	2017-06-23 22:37:26.929+03	ABONAMENT	41	0.00
Popescu	Andrei	10000000014	2017-05-25 11:22:08.772596+03	\N	PORTOFEL	\N	0.00
Popescu	Andrei	10000000013	2017-05-25 11:22:08.77261+03	\N	PORTOFEL	\N	0.00
Popescu	Andrei	10000000015	2017-05-25 11:22:57.190479+03	2017-05-26 14:26:01.775+03	ABONAMENT	40	2.10
\.


--
-- TOC entry 2151 (class 0 OID 0)
-- Dependencies: 186
-- Name: tcard_cod_card_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcard_cod_card_seq', 10000000015, true);


--
-- TOC entry 2139 (class 0 OID 16425)
-- Dependencies: 187
-- Data for Name: tvalidare; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tvalidare (data_validare, id_validare, cod_card, cod_masina, linie) FROM stdin;
2017-05-17 00:20:00+03	1	10000000008	32	1
2017-05-17 00:19:00+03	2	10000000008	321	42
2017-05-17 00:19:00+03	3	10000000011	321	42
2017-05-17 00:19:00+03	4	10000000011	321	42
2017-05-17 00:19:00+03	5	10000000011	321	42
2017-05-17 00:19:00+03	6	10000000011	321	42
2017-05-17 00:37:00+03	7	10000000008	324	1
2017-05-17 00:37:00+03	8	10000000011	324	1
2017-05-17 00:37:00+03	9	10000000011	324	1
2017-05-17 00:37:00+03	10	10000000011	324	1
2017-05-17 00:37:00+03	11	10000000011	324	1
2017-05-17 00:37:00+03	12	10000000011	324	1
2017-05-17 00:37:00+03	13	10000000011	324	1
2017-05-17 00:37:00+03	14	10000000011	324	1
2017-05-17 00:37:00+03	15	10000000011	324	1
2017-05-17 00:37:00+03	16	10000000011	324	1
2017-05-17 00:37:00+03	17	10000000011	324	1
2017-05-17 00:37:00+03	18	10000000011	324	1
2017-05-17 00:37:00+03	19	10000000011	324	1
2017-05-17 00:37:00+03	20	10000000011	324	1
2017-05-17 00:37:00+03	21	10000000011	324	1
2017-05-17 00:37:00+03	22	10000000011	324	1
2017-05-17 00:37:00+03	23	10000000011	324	1
2017-05-17 00:37:00+03	24	10000000011	324	1
2017-05-17 00:37:00+03	25	10000000011	324	1
2017-05-17 00:37:00+03	26	10000000011	324	1
2017-05-17 00:37:00+03	27	10000000011	324	1
2017-05-17 00:37:00+03	28	10000000011	324	1
2017-05-17 00:37:00+03	29	10000000011	324	1
2017-05-17 00:37:00+03	30	10000000011	324	1
2017-05-17 00:37:00+03	31	10000000011	324	1
2017-05-17 00:37:00+03	32	10000000011	324	1
2017-05-17 00:37:00+03	33	10000000011	324	1
2017-06-22 22:35:00+03	34	10000000012	33	41
2017-05-25 14:22:00+03	35	10000000015	542	40
2017-05-25 14:22:00+03	36	10000000015	542	40
2017-05-25 14:22:00+03	37	10000000015	542	40
2017-05-25 14:22:00+03	38	10000000015	541	40
\.


--
-- TOC entry 2152 (class 0 OID 0)
-- Dependencies: 188
-- Name: tvalidat_id_validare_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tvalidat_id_validare_seq', 38, true);


--
-- TOC entry 2015 (class 2606 OID 16436)
-- Name: tcard cod_card_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcard
    ADD CONSTRAINT cod_card_pk PRIMARY KEY (cod_card);


--
-- TOC entry 2018 (class 2606 OID 16438)
-- Name: tvalidare tvalidata_id_validare_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tvalidare
    ADD CONSTRAINT tvalidata_id_validare_pk PRIMARY KEY (id_validare);


--
-- TOC entry 2016 (class 1259 OID 16439)
-- Name: tvalidat_id_validare_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX tvalidat_id_validare_uindex ON tvalidare USING btree (id_validare);


--
-- TOC entry 2019 (class 2606 OID 16440)
-- Name: tvalidare tvalidat_tcard_cod_card_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tvalidare
    ADD CONSTRAINT tvalidat_tcard_cod_card_fk FOREIGN KEY (cod_card) REFERENCES tcard(cod_card);


--
-- TOC entry 2147 (class 0 OID 0)
-- Dependencies: 3
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-05-26 18:39:11

--
-- PostgreSQL database dump complete
--

